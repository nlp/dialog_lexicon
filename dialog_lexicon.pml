<?xml version="1.0" encoding="UTF-8" ?>
<Package name="dialog_presentation_nlp" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior_1" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="skin_B2B" src="skin_B2B/skin_B2B.dlg" />
        <Dialog name="skin_B2C" src="skin_B2C/skin_B2C.dlg" />
        <Dialog name="skin_tmp_tweak" src="skin_tmp_tweak/skin_tmp_tweak.dlg" />
	<Dialog name="lexicon" src="lexicon.dlg" />
	<Dialog name="lexicon_geography" src="lexicon_geography.dlg" />
	<Dialog name="lexicon_names" src="lexicon_names.dlg" />
	<Dialog name="lexicon_time" src="lexicon_time.dlg" />
    </Dialogs>
    <Resources>
        <File name="icon" src="icon.png" />
	<File name="translation_de_DE" src="translations/translation_de_DE.qm" />
	<File name="translation_en_US" src="translations/translation_en_US.qm" />
	<File name="translation_es_ES" src="translations/translation_es_ES.qm" />
	<File name="translation_fr_FR" src="translations/translation_fr_FR.qm" />
	<File name="translation_it_IT" src="translations/translation_it_IT.qm" />
	<File name="translation_ja_JP" src="translations/translation_ja_JP.qm" />
	<File name="ALSetDateTime" src="ALSetDateTime.py" />
	<File name="resetVariable" src="resetVariable.py" />
    </Resources>
    <Topics>
        <Topic name="skin_B2B_enu" src="skin_B2B/skin_B2B_enu.top" topicName="skin_B2B" language="en_US" />
        <Topic name="skin_B2B_frf" src="skin_B2B/skin_B2B_frf.top" topicName="skin_B2B" language="fr_FR" />
        <Topic name="skin_B2B_jpj" src="skin_B2B/skin_B2B_jpj.top" topicName="skin_B2B" language="ja_JP" />
        <Topic name="skin_B2C_jpj" src="skin_B2C/skin_B2C_jpj.top" topicName="skin_B2C" language="ja_JP" />
        <Topic name="skin_tmp_tweak_enu" src="skin_tmp_tweak/skin_tmp_tweak_enu.top" topicName="skin_tmp_tweak" language="en_US" />
        <Topic name="skin_tmp_tweak_frf" src="skin_tmp_tweak/skin_tmp_tweak_frf.top" topicName="skin_tmp_tweak" language="fr_FR" />
        <Topic name="skin_tmp_tweak_jpj" src="skin_tmp_tweak/skin_tmp_tweak_jpj.top" topicName="skin_tmp_tweak" language="ja_JP" />
        <Topic name="lexicon_enu" src="lexicon_enu.top" topicName="lexicon" language="en_US" />
        <Topic name="lexicon_czc" src="lexicon_czc.top" topicName="lexicon" language="cs_CZ" />
        <Topic name="lexicon_frf" src="lexicon_frf.top" topicName="lexicon" language="fr_FR" />
        <Topic name="lexicon_ged" src="lexicon_ged.top" topicName="lexicon" language="de_DE" />
        <Topic name="lexicon_geography_enu" src="lexicon_geography_enu.top" topicName="lexicon_geography" language="en_US" />
        <Topic name="lexicon_geography_czc" src="lexicon_geography_czc.top" topicName="lexicon_geography" language="cs_CZ" />
        <Topic name="lexicon_geography_frf" src="lexicon_geography_frf.top" topicName="lexicon_geography" language="fr_FR" />
        <Topic name="lexicon_iti" src="lexicon_iti.top" topicName="lexicon" language="it_IT" />
        <Topic name="lexicon_jpj" src="lexicon_jpj.top" topicName="lexicon" language="ja_JP" />
        <Topic name="lexicon_names_enu" src="lexicon_names_enu.top" topicName="lexicon_names" language="en_US" />
        <Topic name="lexicon_names_czc" src="lexicon_names_czc.top" topicName="lexicon_names" language="cs_CZ" />
        <Topic name="lexicon_names_frf" src="lexicon_names_frf.top" topicName="lexicon_names" language="fr_FR" />
        <Topic name="lexicon_spe" src="lexicon_spe.top" topicName="lexicon" language="es_ES" />
        <Topic name="lexicon_time_enu" src="lexicon_time_enu.top" topicName="lexicon_time" language="en_US" />
        <Topic name="lexicon_time_czc" src="lexicon_time_czc.top" topicName="lexicon_time" language="cs_CZ" />
        <Topic name="lexicon_time_frf" src="lexicon_time_frf.top" topicName="lexicon_time" language="fr_FR" />
        <Topic name="lexicon_time_jpj" src="lexicon_time_jpj.top" topicName="lexicon_time" language="ja_JP" />
    </Topics>
    <IgnoredPaths>
        <Path src=".metadata" />
    </IgnoredPaths>
</Package>
