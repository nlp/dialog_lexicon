# Czech lexicon

Developed at [NLP Centre](https://nlp.fi.muni.cz/en), [FI MU](https://www.fi.muni.cz/index.html.en) for [Karel Pepper](https://nlp.fi.muni.cz/trac/pepper)

This is just an extension of original `dialog_lexicon` application
from Softbank Robotics.

## Installation

* you need to have the `dialog_lexicon` application installed
* copy `/home/nao/.local/share/PackageManager/apps/dialog_lexicon` to
  new place
* copy files from this project over the files from `dialog_lexicon`
* [make and install](https://nlp.fi.muni.cz/trac/pepper/wiki/InstallPkg) the new package over the previous version

